package com.acooly.openapi.client.provider.cj;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.provider.cj.CjApiService;
import com.acooly.module.openapi.client.provider.cj.CjProperties;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchDeductRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchDeductResponse;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchQueryRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchQueryResponse;
import com.acooly.module.openapi.client.provider.cj.message.CjRealDeductRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjRealDeductResponse;
import com.acooly.module.openapi.client.provider.cj.message.CjRealQueryRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjRealQueryResponse;
import com.acooly.module.openapi.client.provider.cj.message.dto.CjDeductBodyInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@BootApp(sysName = "CjTest")
public class CjTest extends NoWebTestBase {

    @Autowired
    private CjApiService cjApiService;
    @Autowired
    private CjProperties cjProperties;

    String oid = Ids.gid();

    /**
     * BankDeduct:畅捷单笔代扣
     */
    @Test
    public void testRealDeduct(){

        CjRealDeductRequest request = new CjRealDeductRequest();

        request.setMertid(cjProperties.getMertid());
        request.setBusinessCode(cjProperties.getBusinessCode());
        request.setProductCode(cjProperties.getProductCode());
        request.setCorpAccNo(cjProperties.getCorpAccNo());

        request.setTrxCode("G10016");
        request.setVersion("01");

        request.setReqSn(oid);
        request.setProtocolNo(oid);
        request.setAccountProp("0");
        request.setBankGeneralName("农业银行");
        request.setAccountNo("6222023801230093000");
        request.setAccountName("傅枫15");
        request.setBankName("农业银行");
        request.setBankCode("103100000026");
        request.setDrctBankCode("103100000026");
        request.setCurrency("CNY");
        request.setAmount("40");
        request.setIdType("0");

        try {
            CjRealDeductResponse response = cjApiService.cjRealDeduct(request);
            System.out.println("畅捷单笔代扣测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }


    /**
     * BankDeduct:畅捷批量代扣
     */
    @Test
    public void testBatchDeduct(){

        CjBatchDeductRequest request = new CjBatchDeductRequest();
        request.setMertid(cjProperties.getMertid());
        request.setBusinessCode(cjProperties.getBusinessCode());
        request.setProductCode(cjProperties.getProductCode());
        request.setCorpAccNo(cjProperties.getCorpAccNo());

        request.setTrxCode("G10003");
        request.setVersion("01");
        request.setTotalCnt(2);
        request.setTotalAmt(Money.cent(200));
        request.setReqSn(Ids.oid());
        request.setAccountProp("0");//对私对公
        request.setTimeliness("0");//时效性

        List<CjDeductBodyInfo> cjDeductBodyInfos = new ArrayList<CjDeductBodyInfo>();

        for(int i = 0; i<2 ; i++){
            CjDeductBodyInfo body = new CjDeductBodyInfo();
            body.setBizOrderNo(Ids.oid());
            body.setPayBankCardNo("6222023801233297860");
            body.setPayRealName("傅枫15");
            body.setPayBankName("农业银行");
            body.setDrctBankCode("103100000026");
            body.setCurrency("CNY");
            body.setAmount(Money.cent(100));
            body.setProtocolNo("o17121915433740210001");
            body.setPayCertNo("500223199111086316");
            body.setPayMobileNo("18288893996");
            cjDeductBodyInfos.add(body);
        }
        request.setCjDeductBodyInfos(cjDeductBodyInfos);
        try {
            CjBatchDeductResponse response = cjApiService.cjBatchDeduct(request);
            System.out.println("畅捷批量代扣测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }


    /**
     * 畅捷单笔代扣查询
     */
    @Test
    public void testCJRealDeductQuery(){

        CjRealQueryRequest request = new CjRealQueryRequest();

        request.setMertid(cjProperties.getMertid());
        request.setTrxCode("G20001");
        request.setVersion("01");
        request.setTimestamp("20171219193512");

        request.setReqSn(Ids.oid());
        request.setQryReqSn("o17121919340073120003b");
        try {
            CjRealQueryResponse response = cjApiService.cjRealDeductQuery(request);
            System.out.println("畅捷单笔代扣测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

    /**
     * 畅捷批量代扣查询
     */
    @Test
    public void testCJBatchDeductQuery(){

        CjBatchQueryRequest request = new CjBatchQueryRequest();

        request.setMertid(cjProperties.getMertid());
        request.setTrxCode("G20002");
        request.setVersion("01");
        request.setTimestamp("20171219193512");

        request.setReqSn(Ids.oid());
        request.setQryReqSn("o17122000334309370004");
        try {
            CjBatchQueryResponse response = cjApiService.cjBatchDeductQuery(request);
            System.out.println("畅捷单笔代扣测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }
}

