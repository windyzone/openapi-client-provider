/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.fuiou;

import com.acooly.core.utils.Strings;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.fuiou.OpenAPIClientFuiouProperties.PREFIX;


/**
 * @author zhangpu@acooly.cn
 */
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientFuiouProperties {
    public static final String PREFIX = "acooly.openapi.client.fuiou";

    /**
     * 网关地址
     */
    private String gateway;

    /**
     * 公钥文件路径
     */
    private String publicKeyPath;

    /**
     * 私钥文件路径
     */
    private String privateKeyPath;


    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 报文中签名字段名称
     */
    private String signatrueParamKey;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;

    /**
     * 商户号
     */
    private String partnerId;


    /**
     * 异步通知URL前缀
     */
    private String notifyUrlPrefix;
    /**
     * WEB充值同步通知地址
     */
    private String depositReturnUrl;
    /**
     * APP充值同步通知地址
     */
    private String appDepositReturnUrl;
    /**
     * WEB提现同步通知地址
     */
    private String withdrawRerurnUrl;
    /**
     * APP提现同步通知地址
     */
    private String appWithdrawRerurnUrl;


    public String getNotifyUrl(String service) {
        return getCanonicalUrl(this.domain, getCanonicalUrl(notifyUrlPrefix, service));
    }

    public String getReturnUrl(String url) {
        return getCanonicalUrl(this.domain, url);
    }


    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }


    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getPublicKeyPath() {
        return publicKeyPath;
    }

    public void setPublicKeyPath(String publicKeyPath) {
        this.publicKeyPath = publicKeyPath;
    }

    public String getPrivateKeyPath() {
        return privateKeyPath;
    }

    public void setPrivateKeyPath(String privateKeyPath) {
        this.privateKeyPath = privateKeyPath;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSignatrueParamKey() {
        return signatrueParamKey;
    }

    public void setSignatrueParamKey(String signatrueParamKey) {
        this.signatrueParamKey = signatrueParamKey;
    }

    public long getConnTimeout() {
        return connTimeout;
    }

    public void setConnTimeout(long connTimeout) {
        this.connTimeout = connTimeout;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getNotifyUrlPrefix() {
        return notifyUrlPrefix;
    }

    public void setNotifyUrlPrefix(String notifyUrlPrefix) {
        this.notifyUrlPrefix = notifyUrlPrefix;
    }

    public String getDepositReturnUrl() {
        return depositReturnUrl;
    }

    public void setDepositReturnUrl(String depositReturnUrl) {
        this.depositReturnUrl = depositReturnUrl;
    }

    public String getAppDepositReturnUrl() {
        return appDepositReturnUrl;
    }

    public void setAppDepositReturnUrl(String appDepositReturnUrl) {
        this.appDepositReturnUrl = appDepositReturnUrl;
    }

    public String getWithdrawRerurnUrl() {
        return withdrawRerurnUrl;
    }

    public void setWithdrawRerurnUrl(String withdrawRerurnUrl) {
        this.withdrawRerurnUrl = withdrawRerurnUrl;
    }

    public String getAppWithdrawRerurnUrl() {
        return appWithdrawRerurnUrl;
    }

    public void setAppWithdrawRerurnUrl(String appWithdrawRerurnUrl) {
        this.appWithdrawRerurnUrl = appWithdrawRerurnUrl;
    }
}
