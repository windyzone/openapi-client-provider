/**
 * create by zhangpu
 * date:2015年3月11日
 */
package com.acooly.module.openapi.client.provider.fuiou.domain;

import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;

import javax.validation.constraints.Size;

/**
 * @author zhangpu
 * 
 */
public class FuiouNotify extends FuiouResponse {

	/** 交易日期 20140101 */
	@Size(min = 8, max = 8)
	@FuiouAlias("mchnt_txn_dt")
	private String mchntTxnDt;

	public String getMchntTxnDt() {
		return mchntTxnDt;
	}

	public void setMchntTxnDt(String mchntTxnDt) {
		this.mchntTxnDt = mchntTxnDt;
	}

}
