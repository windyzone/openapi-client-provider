/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:32:16 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.query;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:32:16
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.QUERY_TRADE ,type = ApiMessageType.Request)
public class QueryTradeRequest extends FudianRequest {

    /**
     * 查询订单日期
     * 查询订单的日期，辅助标识
     */
    @NotEmpty
    @Length(min = 20,max=20)
    private String queryOrderNo;

    /**
     * 查询订单流水号
     * 查询订单的唯一标识
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String queryOrderDate;

    /**
     * 交易类型
     * 取值范围：01充值，02提现，03投标，04借款人还款，05投资人回款，06债权认购，07满标放款
     */
    @NotEmpty
    @Length(min = 2,max=2)
    private String queryType;
}