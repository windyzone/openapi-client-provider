/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.fudian.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.provider.fudian.FudianConstants;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhangpu
 */
@Slf4j
@Component
public class FudianNotifyUnmarshall extends FudianAbstractMarshall implements ApiUnmarshal<FudianNotify, Map<String, String>> {
    
    @Override
    public FudianNotify unmarshal(Map<String, String> message, String serviceKey) {
        String crypt = message.get(FudianConstants.REQUEST_PARAM_NAME);
        return (FudianNotify) doUnMarshall(crypt, serviceKey, true);
    }
}
