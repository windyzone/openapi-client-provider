/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 03:07:09 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-10 03:07:09
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.BUSINESS_AUTHORIZATION ,type = ApiMessageType.Notify)
public class BusinessAuthorizationNotify extends FudianNotify {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 业务类型
     * 1授权
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String businessType;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 授权状态
     * 1：成功授权；2：取消授权
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String status;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;

    /**
     * 授权金额
     */
    @NotEmpty
    private String amount;

    /**
     * 授权截至时间
     */
    private String endTime;
}