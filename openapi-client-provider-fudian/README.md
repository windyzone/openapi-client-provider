<!-- title: 富滇银行支付SDK组件 -->
<!-- type: gateway -->
<!-- author: zhike -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
    <artifactId>openapi-client-provider</artifactId>
    <groupId>com.acooly</groupId>
    <version>4.2.0-SNAPSHOT</version>
</dependency>
```

## 配置文件

```properties
# 开关
acooly.openapi.client.fudian.enable=true
# 商户号
acooly.openapi.client.fudian.merchantNo=M02538118117201001
# 网关地址
acooly.openapi.client.fudian.gatewayUrl=https://ebank.richdian.com:8443/
# 商户证书位置，支持文件物理路径和classpath:...方式
acooly.openapi.client.fudian.keystore=classpath:keystore/fudian/p2p.pfx
# 商户证书库保护密码
acooly.openapi.client.fudian.keystorePswd=123456
# 银行证书
acooly.openapi.client.fudian.gatewayCert=classpath:keystore/fudian/bank.cer
# 本平台的域名（包含端口），用于自动组装notifyUrl
acooly.openapi.client.fudian.domain=http://218.70.106.250:9090
# notifyUrl的公共前缀，用于filter同一拦截处理
acooly.openapi.client.fudian.notifyUrl=/openapi/client/fudian/notify
# 网络超时参数
acooly.openapi.client.fudian.connTimeout=10000
acooly.openapi.client.fudian.readTimeout=30000

## 对账文件下载参数
acooly.openapi.client.fudian.checkfile.host=acooly.cn
acooly.openapi.client.fudian.checkfile.port=23987
acooly.openapi.client.fudian.checkfile.username=mysftp
acooly.openapi.client.fudian.checkfile.password=Zp123456
acooly.openapi.client.fudian.checkfile.serverRoot=/upload
# 下载后的本地存储路径
acooly.openapi.client.fudian.checkfile.localRoot=
```


# 接口服务

## Fudian服务模式

请在目标集成系统中，直接注入FudianApiService服务，调用对应的方法直接处理。

```java
com.acooly.module.openapi.client.provider.fudian.FudianApiService
```

使用案例：openapi-client-test模块下的：com.acooly.openapi.client.provider.fudian.FudianTest

## client模式

在目标集成系统中，注入客户端工具：FudianApiServiceClient

```java
com.acooly.module.openapi.client.provider.fudian.FudianApiServiceClient
```

# 文件下载

SDK框架提供了通用的文件下载后进行处理的程序封装。

封装能力：

1. 文件的SFTP下载，配置SFTP参数，服务器和本地文件路径。传入参数: FileHandlerOrder(Map,period,name[标识])
2. 文件下载后，按制定批次大小读取，然后交给FileDataHandler的实现类处理


主要特点。

１. FileHanlder接口提供文件处理的通用方法，其中Fudian的具体实现请直接使用：FudianSftpFileHandler。主要分两个步骤：先下载然后读取文件处理

```java
com.acooly.module.openapi.client.provider.fudian.file.FudianSftpFileHandler
```
2. 下载并处理的接口方法为：downloadHandle,如下：

```java
void downloadHandle(FileHandlerOrder order, FileDataHandler fileDataHandler);

/**
* 根据order里面的name自动匹配spring容器里面的FileDataHanlder的实现
*/
void downloadHandle(FileHandlerOrder order);
```

其中FileHanlderOrder主要提供外部传入的参数，用于生成服务器端的下载文件名和路径，以及本地文件的生成规则。这由具体的实现类实现，在fudian中已经具体实现在FudianSftpFileHandler中。
主要参数包括：period(账期),fileType(业务唯一标识)和一个Map(你可以任意传入，传入文件名都可以)

3. FileDataHandler是文件下载完成后的处理接口。具体使用方法请参考：openapi-client-test下的：FudianFileHandlerTest

```java
com.acooly.openapi.client.provider.fudian.file.FudianFileHandlerTest
```