package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 16:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankRegisterQueryRequestBody implements Serializable {
    /**
     *合作方机构号（网商银行分配）
     */
    @Size(max = 64)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 申请单号
     */
    @Size(max = 64)
    @NotBlank
    @XStreamAlias("OrderNo")
    private String orderNo;

}


