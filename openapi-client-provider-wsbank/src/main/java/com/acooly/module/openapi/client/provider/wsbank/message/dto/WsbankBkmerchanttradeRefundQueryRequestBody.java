package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankBkmerchanttradeRefundQueryRequestBody implements Serializable {

	private static final long serialVersionUID = -4957566653955247068L;

	/**
	 * 合作方机构号（网商银行分配）
	 */
	@Size(max = 64)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;

	/**
	 * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
	 */
	@Size(max = 64)
	@XStreamAlias("MerchantId")
	@NotBlank
	private String merchantId;

	/**
	 * 退款外部交易号。由合作方生成，同笔退款交易，交易状态未明需要重试时，使用同一个交易号。
	 */
	@Size(max = 64)
	@XStreamAlias("OutRefundNo")
	@NotBlank
	private String outRefundNo;

}
