package com.acooly.module.openapi.client.provider.wsbank.message.base;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author weili 2018/5/22 18:14
 */
@Getter
@Setter
public class WsbankSiteInfo implements Serializable {

	
	//3、SiteInfo线上站点信息为json数组的map形式，包含SiteType为key和SiteUrl为value，SiteType最大32位，SiteUrl最大256位
	@JSONField(name = "SiteType")
	private String SiteType;
	
	@JSONField(name = "SiteUrl")
	private String SiteUrl;
	
	 @Override
	public String toString() {
	        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
