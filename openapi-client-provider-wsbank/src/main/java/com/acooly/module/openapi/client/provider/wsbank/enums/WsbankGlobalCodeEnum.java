package com.acooly.module.openapi.client.provider.wsbank.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 商户类型枚举
 * @author sunjx
 *
 */
public enum WsbankGlobalCodeEnum implements Messageable {

    SGW01002("SGW01002","系统未知异常[重试]"),
    SGW11009("SGW11009","签名异常	[发起查询]"),
    SGW12001("SGW12001","接收到空报文[检查发送报文是否正确]"),
    SGW12014("SGW12014","报文解析错误[检查发送报文是否正确]"),
    SGW12016("SGW12016","验签错误	[检查发送报文的签名是否正确]"),
    SGW21008("SGW21008","业务系统处理超时[发起查询]"),
    SGW21010("SGW21010","无法找到报文对应的业务系统[网关配置错误，联系网商银行]"),
    SGW21013("SGW21013","调用内部业务系统异常[发起查询]"),
    SGW31001("SGW31001","重复报文	[发起查询]"),
    SGW31004("SGW31004","网关交易类型不存在[网关配置错误，联系网商银行]"),
    SGW31005("SGW31005","网关外部交易编码不存在	[网关配置错误，联系网商银行]"),
    SGW81000("SGW81000","非法参数	[检查发送报文是否正确]"),
    SGW81001("SGW81001","报文转换异常[检查发送报文是否正确]"),
    SGW13012("SGW13012","报文参数错误[检查发送报文是否正确]"),
    ;

    private final String code;
    private final String message;

    WsbankGlobalCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (WsbankGlobalCodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static WsbankGlobalCodeEnum find(String code) {
        for (WsbankGlobalCodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<WsbankGlobalCodeEnum> getAll() {
        List<WsbankGlobalCodeEnum> list = new ArrayList<WsbankGlobalCodeEnum>();
        for (WsbankGlobalCodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (WsbankGlobalCodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }
}
