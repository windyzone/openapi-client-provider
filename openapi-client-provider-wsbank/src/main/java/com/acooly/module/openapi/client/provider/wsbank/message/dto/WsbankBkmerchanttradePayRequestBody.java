package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 16:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankBkmerchanttradePayRequestBody implements Serializable {

	/**
	 * 第三方支付授权码。扫码支付授权码，设备读取用户支付宝或微信中的条码或者二维码信息
	 */
	@Size(max = 64)
	@XStreamAlias("AuthCode")
	@NotBlank
	private String authCode;

	/**
	 * 外部交易号。由合作方系统生成，只能包含字母、数字、下划线；需保证合作方系统不重复。
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;

	/**
	 * 商品描述。该信息将透传至第三方支付公司系统，并在客户端明细中展示。格式要求：店名-销售商品类目
	 */
	@Size(max = 128)
	@XStreamAlias("Body")
	@NotBlank
	private String body;

	/**
	 * 商品标记。微信支付代金券或立减优惠功能的参数。
	 */
	@Size(max = 32)
	@XStreamAlias("GoodsTag")
	private String goodsTag;

	/**
	 * 商品详情列表。支付宝单品优惠功能字段，JSON格式，会透传至第三方支付。
	 */
	@XStreamAlias("GoodsDetail")
	private String goodsDetail;

	/**
	 * 交易总额度。货币最小单位，人民币：分
	 */
	@XStreamAlias("TotalAmount")
	@NotBlank
	private String totalAmount;

	/**
	 * 币种。默认CNY。
	 */
	@XStreamAlias("Currency")
	@NotBlank
	private String currency;

	/**
	 * 合作方机构号（网商银行分配）
	 */
	@Size(max = 64)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;

	@Size(max = 64)
	@XStreamAlias("MerchantId")
	@NotBlank
	private String merchantId;

	/**
	 * 支付渠道类型。该笔支付走的第三方支付渠道。可选值： ALI：支付宝 WX：微信支付 QQ：手机QQ（暂未开放） JD：京东钱包（暂未开放）
	 */
	@Size(max = 64)
	@XStreamAlias("ChannelType")
	@NotBlank
	private String channelType;

	/**
	 * 操作员ID。门店操作员ID
	 */
	@Size(max = 32)
	@XStreamAlias("OperatorId")
	private String operatorId;

	/**
	 * 门店ID。门店ID
	 */
	@XStreamAlias("StoreId")
	@Size(max = 32)
	private String storeId;

	/**
	 * 终端设备号。门店收银设备ID
	 */
	@XStreamAlias("DeviceId")
	@Size(max = 32)
	private String deviceId;

	@XStreamAlias("DeviceCreateIp")
	@NotBlank
	@Size(max = 16)
	private String deviceCreateIp;

	/**
	 * 订单有效期。指定订单的支付有效时间（以分钟计算），超过有效时间用户将无法支付。若不指定该参数则系统默认设置1小时支付有效时间。参数允许设置范围：1
	 * -1440区间的整数值。
	 */
	@XStreamAlias("ExpireExpress")
	private int expireExpress;

	/**
	 * 清算方式。可选值： T0：T+0清算按笔清算，目前仅支持清算到余利宝，不支持清算到银行卡。
	 * T1：T+1汇总清算，可支持清算到余利宝及清算到银行卡。
	 */
	@XStreamAlias("SettleType")
	@NotBlank
	@Size(max = 32)
	private String settleType;

	/**
	 * 附加信息，原样返回。
	 */
	@XStreamAlias("Attach")
	@Size(max = 128)
	private String attach;

	/**
	 * 禁用支付方式。商户禁受理支付方式列表，多个用逗号隔开。可选值： credit：信用卡 pcredit：花呗（仅支付宝）
	 */
	@XStreamAlias("PayLimit")
	@Size(max = 32)
	private String payLimit;

	/**
	 * 可打折金额。货币最小单位，人民币：分。仅支付宝交易有效。如果同时传入了【可打折金额】，【不可打折金额】，【订单总金额】三者，则必须满足如下条件：【
	 * 交易总额度】=【可打折金额】+【不可打折金额】 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("DiscountableAmount")
	private String discountableAmount;

	/**
	 * 不可打折金额。货币最小单位，人民币：分。仅支付宝交易有效。如果同时传入了【可打折金额】，【不可打折金额】，【订单总金额】三者，则必须满足如下条件：
	 * 【交易总额度】=【可打折金额】+【不可打折金额】 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("UndiscountableAmount")
	private String undiscountableAmount;

	/**
	 * 支付宝的店铺编号，用于精准店铺营销 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("AlipayStoreId")
	@Size(max = 32)
	private String alipayStoreId;

	/**
	 * 支付宝支持系统商返佣，该参数作为系统商返佣数据提取的依据，请填写系统商签约协议的PID 该字段2017年9月30日提供线上服务。
	 */
	@XStreamAlias("SysServiceProviderId")
	@Size(max = 64)
	private String sysServiceProviderId;

	/**
	 * 花呗交易分期数，可选值： 3：3期 6：6期 12：12期
	 * 每期间隔为一个月。例如，选择3期，所垫付的资金及利息按3个月等额本息还款，每月还款一笔。
	 */
	@XStreamAlias("CheckLaterNm")
	private String checkLaterNm;
}
