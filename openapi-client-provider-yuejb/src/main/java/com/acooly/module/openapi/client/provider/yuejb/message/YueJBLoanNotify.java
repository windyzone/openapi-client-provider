package com.acooly.module.openapi.client.provider.yuejb.message;

import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBNotify;
import lombok.Data;

/**
 * Created by ouwen@yiji.com} on 2017/11/7.
 */
@Data
public class YueJBLoanNotify extends YueJBNotify {
    /**
     * 放款订单号
     */
    private String loanNo;
}
