/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.alipay.domain;

import com.acooly.module.openapi.client.provider.alipay.support.AlipayAlias;

import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 * @author zhangpu
 */
@Getter
@Setter
public class AlipayResponse extends AlipayApiMessage {

	/** 支付宝交易号 */
	@AlipayAlias(value = "trade_no")
	private String tradeNo;
	
	/** 商户订单号 */
	@AlipayAlias(value = "out_trade_no")
	private String outTradeNo;
	
	/** 返回码 */
	@AlipayAlias(value = "code")
	private String code;
	
	/** 返回码信息 */
	@AlipayAlias(value = "msg")
	private String msg;
	
	/** 返回子编码 */
	@AlipayAlias(value = "sub_code")
	private String subCode;
	
	/** 返回子编码信息 */
	@AlipayAlias(value = "sub_msg")
	private String subMsg;
}
