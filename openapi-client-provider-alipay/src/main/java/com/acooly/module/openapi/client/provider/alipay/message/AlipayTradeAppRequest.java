package com.acooly.module.openapi.client.provider.alipay.message;

import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayApiMsgInfo;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayRequest;
import com.acooly.module.openapi.client.provider.alipay.enums.AlipayServiceEnum;

import lombok.Getter;
import lombok.Setter;

@AlipayApiMsgInfo(service=AlipayServiceEnum.PAY_ALIPAY_TRADE_APP,type=ApiMessageType.Request)
@Setter
@Getter
public class AlipayTradeAppRequest extends AlipayRequest{

	/**
	 * 交易名称/商品名称
	 */
	private String subject;
	/**
	 * 交易金额
	 */
	private Money amount;
	
	/**
	 * 商品描述
	 */
	private String body;
	/**
	 * 订单过期时间
	 * <li>取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）
	 */
	private String timeoutExpress;
}
