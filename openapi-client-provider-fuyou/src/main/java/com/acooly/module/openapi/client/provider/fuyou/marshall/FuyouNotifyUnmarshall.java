/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.fuyou.marshall;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.fuyou.FuyouConstants;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouNotify;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouRespCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

/**
 * @author zhangpu
 */
@Service
public class FuyouNotifyUnmarshall extends FuyouMarshallSupport
        implements ApiUnmarshal<FuyouNotify, Map<String, String>> {
    private static final Logger logger = LoggerFactory.getLogger(FuyouNotifyUnmarshall.class);
    @Resource(name="fuyouMessageFactory")
    private MessageFactory messageFactory;

    @SuppressWarnings("unchecked")
    @Override
    public FuyouNotify unmarshal(Map<String, String> message, String serviceName) {
        try {
            logger.info("异步通知{}", message);
            String signature = message.get(FuyouServiceEnum.find(serviceName).getSignKey());
            FuyouNotify notify = doUnmarshall(message,serviceName);
            notify.setPartner(getProperties().getPartnerId());
            notify.setKey(getSignKey(serviceName));
            String plain = notify.getSignStr();
            doVerify(plain,signature,serviceName);
            return notify;
        } catch (Exception e) {
            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
        }

    }

    protected FuyouNotify doUnmarshall(Map<String, String> message, String serviceName) {
        FuyouNotify notify = (FuyouNotify) messageFactory.getNotify(FuyouServiceEnum.find(serviceName).getKey());
        Set<Field> fields = Reflections.getFields(notify.getClass());
        String key = null;
        for (Field field : fields) {
            FuyouAlias fuyouAlias = field.getAnnotation(FuyouAlias.class);
            if (fuyouAlias == null) {
                continue;
            }
            key = fuyouAlias.value();
            if (Strings.isBlank(key)) {
                key = field.getName();
            }
            Reflections.invokeSetter(notify, field.getName(), message.get(key));
        }
        notify.setService(serviceName);
        if (Strings.isNotBlank(notify.getRespCode())) {
            notify.setMessage(FuyouRespCodes.getMessage(notify.getRespCode()));
        }
        return notify;
    }

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }

}
