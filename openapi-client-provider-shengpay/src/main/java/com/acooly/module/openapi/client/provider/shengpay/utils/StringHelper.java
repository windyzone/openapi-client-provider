package com.acooly.module.openapi.client.provider.shengpay.utils;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * @author zhike 2018/4/18 11:00
 */
public class StringHelper {

    /**
     * 组装签名字符串
     * @param jsonMap
     * @return
     */
    public static String getOrignSign(Map<String,String> jsonMap){
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : jsonMap.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        if (sb.length() > 1) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * 判断一个字符串是否是json格式
     * @param content
     * @return
     */
    public static boolean isJson(String content) {

        try {
            JSONObject jsonStr = JSONObject.parseObject(content);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
