package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayResponse;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/17 18:58
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.PAYMENT_CONFIRM,type = ApiMessageType.Response)
public class ShengpayPaymentConfirmResponse extends ShengpayResponse {

    /**
     * 支付状态
     */
    private String paymentStatus;

    /**
     * 商户系统内订单号的唯一标识
     */
    private String merchantOrderNo;

    /**
     * 盛付通系统内针对此商户订单的唯一订单号，如: 20160105105839885474
     */
    private String sftOrderNo;

    /**
     * 订单金额，单位RMB-元，大于0，保留两位小数
     */
    private String orderAmount;

    /**
     * 订单金额，单位RMB-元，大于0，保留两位小数
     */
    private String payableAmount;

    /**
     * 支付完成时间格式为:yyyyMMddHHmmss,如:20110707112233
     */
    private String finishTime;

    /** 如果支付后签约的，该值不为空，否则为空 S：成功 P:处理中 F:签约失败 */
    private String signStatus;

    /**
     * 签约协议号，支付后签约成功的该值不为空
     */
    private String agreementNo;
}
