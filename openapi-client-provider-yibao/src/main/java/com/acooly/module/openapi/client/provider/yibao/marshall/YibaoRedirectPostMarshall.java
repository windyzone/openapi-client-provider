/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 16:11 创建
 */
package com.acooly.module.openapi.client.provider.yibao.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.yibao.YibaoConstants;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 跳转请求报文组装
 *
 * @author zhangpu 2017-09-24 16:11
 */
@Slf4j
@Component
public class YibaoRedirectPostMarshall extends YibaoAbstractMarshall implements ApiMarshal<PostRedirect, YibaoRequest> {

    @Override
    public PostRedirect marshal(YibaoRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        postRedirect.setRedirectUrl(YibaoConstants.getCanonicalUrl(getProperties().getGatewayUrl(),
                YibaoConstants.getServiceName(source)));
        postRedirect.addData(YibaoConstants.REQUEST_PARAM_NAME, doMarshall(source));
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }


}