/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.yibao;

import com.acooly.module.openapi.client.provider.yibao.domain.YibaoNotify;
import com.acooly.module.openapi.client.provider.yibao.message.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Component
public class YibaoApiService {

    @Resource(name = "yibaoApiServiceClient")
    private YibaoApiServiceClient yibaoApiServiceClient;

    /**
     * 有短验绑卡请求
     * @param request
     * @return
     */
    public YibaoSmsBindCardApplyResponse smsBindCardApply(YibaoSmsBindCardApplyRequest request) {
        return (YibaoSmsBindCardApplyResponse)yibaoApiServiceClient.execute(request);
    }

    /**
     * 有短验绑卡请求短验确认
     * @param request
     * @return
     */
    public YibaoSmsBindCardConfirmResponse smsBindCardConfirm(YibaoSmsBindCardConfirmRequest request) {
        return (YibaoSmsBindCardConfirmResponse)yibaoApiServiceClient.execute(request);
    }

    /**
     * 有短验绑卡请求短验重发
     * @param request
     * @return
     */
    public YibaoSmsBindCardResendSmsResponse smsBindCardResendSms(YibaoSmsBindCardResendSmsRequest request) {
        return (YibaoSmsBindCardResendSmsResponse)yibaoApiServiceClient.execute(request);
    }

    /**
     * 无短验充值
     * @param request
     * @return
     */
    public YibaoDirectBindPayResponse directBindPay(YibaoDirectBindPayRequest request) {
        return (YibaoDirectBindPayResponse)yibaoApiServiceClient.execute(request);
    }

    /**
     * 有短验充值请求
     * @param request
     * @return
     */
    public YibaoSmsBindPayApplyResponse smsBindPayApply(YibaoSmsBindPayApplyRequest request) {
        return (YibaoSmsBindPayApplyResponse)yibaoApiServiceClient.execute(request);
    }

    /**
     * 有短验充值确认
     * @param request
     * @return
     */
    public YibaoSmsBindPayConfirmResponse smsBindPayConfirm(YibaoSmsBindPayConfirmRequest request) {
        return (YibaoSmsBindPayConfirmResponse)yibaoApiServiceClient.execute(request);
    }

    /**
     * 有短验充值请求短验重发
     * @param request
     * @return
     */
    public YibaoSmsBindPayResendSmsResponse smsBindPayResendSms(YibaoSmsBindPayResendSmsRequest request) {
        return (YibaoSmsBindPayResendSmsResponse)yibaoApiServiceClient.execute(request);
    }

    /**
     * 首次充值
     * @param request
     * @return
     */
    public YibaoFirstPayApplyResponse firstPayApply(YibaoFirstPayApplyRequest request) {
        return (YibaoFirstPayApplyResponse)yibaoApiServiceClient.execute(request);
    }
    /**
     * 充值订单查询
     * @param request
     * @return
     */
    public YibaoRechargeOrderQueryResponse rechargeOrderQuery(YibaoRechargeOrderQueryRequest request) {
        return (YibaoRechargeOrderQueryResponse)yibaoApiServiceClient.execute(request);
    }

    /**
     * 提现订单查询
     * @param request
     * @return
     */
    public YibaoWithdrawOrderQueryResponse withdrawOrderQuery(YibaoWithdrawOrderQueryRequest request) {
        return (YibaoWithdrawOrderQueryResponse)yibaoApiServiceClient.execute(request);
    }
    /**
     * 同步，异步通知转化
     * serviceKey :YibaoServiceNameEnum枚举中取对应的code
     * @param request 请求
     * @param serviceKey 服务标识
     * @return
     */
    public YibaoNotify notice(HttpServletRequest request, String serviceKey) {
        return  yibaoApiServiceClient.notice(request, serviceKey);
    }
}
