/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.yinsheng;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author zhangpu 2017-09-17 17:49
 */
public class YinShengConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String PROVIDER_NAME = "yinShengProvider";
    public static final String ORDER_NO_NAME = "out_trade_no";
    public static final String BIZ_CONTENT = "biz_content";

    public static final String SIGN = "sign";
    public static final String SIGN_TYPE = "RSA";

    public static final String NETBANK_NAME = "银盛网关";


    /**
     * 宝付网关借记卡对应的payId
     */
    public static final Map<String, String> YS_SERVICE_RESPONSE_MAP =
            new LinkedHashMap<String, String>() {
                {
                    put("ysepay.online.trade.query", "ysepay_online_trade_query_response");
                    put("ysepay.online.directpay.createbyuser", "ysepay_online_directpay_createbyuser_response");
                    put("ysepay.online.fastpay.authorize", "ysepay_online_fastpay_authorize_response");
                    put("ysepay.online.fastpay", "ysepay_online_fastpay_response");
                    put("ysepay.online.bill.downloadurl.get", "ysepay_online_bill_downloadurl_get_response");
                    put("ysepay.online.qrcodepay", "ysepay_online_qrcodepay_response");
                    put("ysepay.online.jsapi.pay", "ysepay_online_jsapi_pay_response");
                    put("ysepay.online.sdkpay", "ysepay_online_sdkpay_response");
                    put("ysepay.merchant.withdraw.d0.accept", "ysepay_merchant_withdraw_d0_accept_response");
                    put("ysepay.merchant.balance.query", "ysepay_merchant_balance_query_response");
                }
            };

}
