/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.hx;

import com.acooly.module.openapi.client.provider.hx.enums.Hx;
import com.acooly.module.openapi.client.provider.hx.enums.HxServiceEnum;
import com.acooly.module.openapi.client.provider.hx.message.HxNetbankPayQueryRequest;
import com.acooly.module.openapi.client.provider.hx.message.HxNetbankPayQueryResponse;
import com.acooly.module.openapi.client.provider.hx.message.HxNetbankPayRequest;
import com.acooly.module.openapi.client.provider.hx.message.HxNetbankPayResponse;
import com.acooly.module.openapi.client.provider.hx.message.HxWithdrawQueryRequest;
import com.acooly.module.openapi.client.provider.hx.message.HxWithdrawQueryResponse;
import com.acooly.module.openapi.client.provider.hx.message.HxWithdrawRequest;
import com.acooly.module.openapi.client.provider.hx.message.HxWithdrawResponse;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Service
public class HxApiService {
	
	@Resource(name = "hxApiServiceClient")
	private HxApiServiceClient hxApiServiceClient;

	/**
	 * 网银
	 * @param request
	 * @return
	 */
	public HxNetbankPayResponse hxNetbankPay(HxNetbankPayRequest request) {
		request.getGateWayReq().getHead().setVersion(Hx.VERSION);
		request.setServiceCode(HxServiceEnum.hxNetbankPay.code());
		return (HxNetbankPayResponse) hxApiServiceClient.execute(request);
	}

	/**
	 * 网银查询
	 * @param request
	 * @return
	 */
	public HxNetbankPayQueryResponse hxNetbankPayQuery(HxNetbankPayQueryRequest request) {
		request.getOrderQueryReq().getHead().setVersion(Hx.VERSION);
		request.setServiceCode(HxServiceEnum.hxNetbankPayQuery.code());
		return (HxNetbankPayQueryResponse) hxApiServiceClient.execute(request);
	}

	/**
	 * 代付
	 * @param request
	 * @return
	 */
	public HxWithdrawResponse hxWithdraw(HxWithdrawRequest request) {
		request.getReqWithdrawHead().setVersion(Hx.VERSION);
		request.setServiceCode(HxServiceEnum.hxWithdraw.code());
		return (HxWithdrawResponse) hxApiServiceClient.execute(request);
	}

	/**
	 * 代付查询
	 * @param request
	 * @return
	 */
	public HxWithdrawQueryResponse hxWithdrawQuery(HxWithdrawQueryRequest request) {
		request.getIssuedTradeReq().getHead().setVersion(Hx.VERSION);
		request.setServiceCode(HxServiceEnum.hxWithdrawQuery.code());
		return (HxWithdrawQueryResponse) hxApiServiceClient.execute(request);
	}
}
