package com.acooly.module.openapi.client.provider.hx.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.hx.domain.HxApiMsgInfo;
import com.acooly.module.openapi.client.provider.hx.domain.HxResponse;
import com.acooly.module.openapi.client.provider.hx.enums.HxServiceEnum;
import com.acooly.module.openapi.client.provider.hx.message.xStream.withdrawQuery.response.IssuedTradeRsp;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Ips")
@HxApiMsgInfo(service = HxServiceEnum.hxWithdrawQuery, type = ApiMessageType.Response)
public class HxWithdrawQueryResponse extends HxResponse {

    @XStreamAlias("IssuedTradeRsp")
    private IssuedTradeRsp issuedTradeRsp;
}
