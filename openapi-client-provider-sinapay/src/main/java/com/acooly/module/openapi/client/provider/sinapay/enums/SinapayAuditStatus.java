package com.acooly.module.openapi.client.provider.sinapay.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 充值状态
 *
 * @author xiaohong
 * @create 2018-07-17 18:12
 **/
public enum SinapayAuditStatus implements Messageable {
    SUCCESS("SUCCESS", "成功"),           //系统会异步通知

    FAILED("FAILED", "失败"),             //系统会异步通知

    PROCESSING("PROCESSING", "处理中");    //系统不会异步通知

    private final String code;
    private final String message;

    SinapayAuditStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (SinapayAuditStatus type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code
     *            查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException
     *             如果 code 没有对应的 Status 。
     */
    public static SinapayAuditStatus find(String code) {
        for (SinapayAuditStatus status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("SinaDepositStatus not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<SinapayAuditStatus> getAll() {
        List<SinapayAuditStatus> list = new ArrayList<SinapayAuditStatus>();
        for (SinapayAuditStatus status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (SinapayAuditStatus status : values()) {
            list.add(status.code());
        }
        return list;
    }

    @Override
    public String toString() {
        return String.format("%s:%s", this.code, this.message);
    }
}
