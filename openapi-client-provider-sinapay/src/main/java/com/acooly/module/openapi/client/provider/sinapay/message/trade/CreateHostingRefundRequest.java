package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.TradePayItem;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * 退款
 *
 * @author xiaohong
 * @create 2018-07-17 16:28
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_HOSTING_REFUND, type = ApiMessageType.Request)
public class CreateHostingRefundRequest extends SinapayRequest {

    /**
     * 交易订单号
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "out_trade_no")
    private String outTradeNo = Ids.oid();

    /**
     * 需要退款的商户订单号
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "orig_outer_trade_no")
    private String origOuterTradeNo;

    /**
     * 退款金额
     * 支持部分退款，退款金额不能大于交易金额。单位为：RMB Yuan，精确到小数点后两位。
     */
    @MoneyConstraint
    @ApiItem(value = "refund_amount")
    private Money refundAmount;

    /**
     * 摘要
     *
     * 退款原因
     */
    @NotEmpty
    @Size(max = 64)
    @ApiItem(value = "summary")
    private String summary;

    /**
     * 分账信息列表（目前代付不支持退款，因此退款时分账列表都为空
     */
    @ApiItem(value = "split_list")
    private List<TradePayItem> splitList = Lists.newArrayList();

    /**
     * 用户IP地址
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "user_ip")
    private String userIp;
}
