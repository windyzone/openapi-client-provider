/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月23日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 会员注册 请求报文
 * 
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_ACTIVATE_MEMBER,type = ApiMessageType.Request)
public class CreateActivateMemberRequest extends SinapayRequest {

	/** 用户标识信息 */
	@Size(max = 50)
	@NotEmpty
	@ApiItem("identity_id")
	private String identityId;

	/** 用户标识类型 */
	@Size(max = 16)
	@NotEmpty
	@ApiItem("identity_type")
	private String identityType = "UID";

	/** 会员类型 1:个人,2:企业 */
	@ApiItem("member_type")
	private String memberType = "1";
	
	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "client_ip")
	private String clientIp;

	public CreateActivateMemberRequest() {
		super();
	}

}
