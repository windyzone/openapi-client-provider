/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.member.dto.BonusInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_BALANCE, type = ApiMessageType.Response)
public class QueryBalanceResponse extends SinapayResponse {

	/** 余额/基金份额 */
	@MoneyConstraint
	private Money balance;

	/** 可用余额/基金份额 */
	@MoneyConstraint
	@ApiItem("available_balance")
	private Money availableBalance;

	/** 存钱罐收益 参数格式：昨日收益^最近一月收益^总收益 */
	private BonusInfo bonus;
}
