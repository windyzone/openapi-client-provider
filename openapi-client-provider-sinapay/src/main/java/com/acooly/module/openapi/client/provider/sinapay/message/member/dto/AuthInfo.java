/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月25日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member.dto;

import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike
 */
@ApiDto
@Getter
@Setter
public class AuthInfo {

	@ItemOrder(0)
	private String authId;
	@ItemOrder(1)
	private String mechantId;
	@ItemOrder(2)
	private String identifyId;
	@ItemOrder(3)
	private String authType;
	@ItemOrder(4)
	private String authSubType;
	@ItemOrder(5)
	private String authStatus;
	@ItemOrder(6)
	private String quota;
	@ItemOrder(7)
	private String quotaDay;
	@ItemOrder(8)
	private String memo;
}
