/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.wewallet.marshall;

import com.google.common.collect.Maps;

import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletResponse;
import com.acooly.module.openapi.client.provider.wewallet.partner.WeWalletKeyPairManager;
import com.acooly.module.openapi.client.provider.wewallet.utils.MapHelper;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;

/**
 * @author zhangpu
 */
@Slf4j
@Service
public class WeWalletResponseUnmarshall extends WeWalletMarshallSupport implements ApiUnmarshal<WeWalletResponse, String> {

    private static final Logger logger = LoggerFactory.getLogger(WeWalletResponseUnmarshall.class);

    @Resource(name = "weWalletMessageFactory")
    private MessageFactory messageFactory;

    @Resource(name = "weWalletGetKeyPairManager")
    private WeWalletKeyPairManager keyPairManager;

    @SuppressWarnings("unchecked")
    @Override
    public WeWalletResponse unmarshal(String message, String serviceName) {
        try {
            logger.info("响应报文:{}", message);
            WeWalletResponse weWalletResponse = (WeWalletResponse) messageFactory.getResponse (serviceName);
            //解析返回的报文
            Map<String, Object>  resMap = parseBody(message);
            MapHelper.mapToBean(resMap,weWalletResponse);
            weWalletResponse.setBizType(serviceName);
            return weWalletResponse;
        } catch (Exception e) {
            throw new ApiClientException("解析响应报文错误:" + e.getMessage());
        }
    }


    public static Map<String, Object> parseBody (String body) {
        if (StringUtils.isBlank (body)) {
            return null;
        }
        Map<String, Object> resultMap = Maps.newHashMap ();
        // body  merId=xx&orderId=xx&bizType=xx&respCode=xx&respMsg=xx
        String[] strs = body.split ("&");
        for (String s : strs) {
            String[] ms = s.split ("=");
            if (ms.length < 2){
                resultMap.put (ms[0],"");
            }else{
                resultMap.put (ms[0], ms[1]);
            }
        }
        return resultMap;
    }
}
