/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-22 13:28 创建
 */
package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.BoscConstants;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscIdCardTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscUserRoleEnum;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author zhangpu 2017-09-22 13:28
 */
@ApiMsgInfo(service = BoscServiceNameEnum.PERSONAL_REGISTER_EXPAND,type = ApiMessageType.Request)
public class PersonalRegisterRequest extends BoscRequest {

    @NotBlank
    @Size(max = 50)
    private String platformUserNo;

    @NotBlank
    @Size(max = 50)
    private String requestNo = Ids.gid();

    @Size(max = 50)
    private String realName;

    @Size(max = 50)
    private String idCardNo;

    @Size(max = 50)
    private String mobile;

    private BoscIdCardTypeEnum idCardType = BoscIdCardTypeEnum.PRC_ID;

    @NotNull
    private BoscUserRoleEnum userRole;
    
    /**
     * 银行卡号
     */
    private String bankcardNo;

    @NotBlank
    @Size(max = 100)
    private String redirectUrl;

    @NotEmpty
    private String checkType = "LIMIT";

    private String userLimitType = "ID_CARD_NO_UNIQUE";

    private String authList = BoscConstants.AUTH_LIST;
    
    public PersonalRegisterRequest() {
        setService(BoscServiceNameEnum.PERSONAL_REGISTER_EXPAND.code());
    }
    
    public PersonalRegisterRequest (String platformUserNo,
                                    BoscUserRoleEnum userRole, String redirectUrl) {
        this();
        this.platformUserNo = platformUserNo;
        this.userRole = userRole;
        this.redirectUrl = redirectUrl;
        this.authList = BoscConstants.ROLE_AUTH_MAPPER.get (userRole);
    }
    
    public PersonalRegisterRequest(String platformUserNo, String realName, String idCardNo, String mobile, BoscUserRoleEnum userRole, String redirectUrl) {
        this(platformUserNo,userRole,redirectUrl);
        this.realName = realName;
        this.idCardNo = idCardNo;
        this.mobile = mobile;
    }

    public String getPlatformUserNo() {
        return platformUserNo;
    }

    public void setPlatformUserNo(String platformUserNo) {
        this.platformUserNo = platformUserNo;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BoscIdCardTypeEnum getIdCardType() {
        return idCardType;
    }

    public void setIdCardType(BoscIdCardTypeEnum idCardType) {
        this.idCardType = idCardType;
    }

    public BoscUserRoleEnum getUserRole() {
        return userRole;
    }

    public void setUserRole(BoscUserRoleEnum userRole) {
        this.userRole = userRole;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getUserLimitType() {
        return userLimitType;
    }

    public void setUserLimitType(String userLimitType) {
        this.userLimitType = userLimitType;
    }

    public String getAuthList() {
        return authList;
    }

    public void setAuthList(String authList) {
        this.authList = authList;
    }
    
    public String getBankcardNo () {
        return bankcardNo;
    }
    
    public void setBankcardNo (String bankcardNo) {
        this.bankcardNo = bankcardNo;
    }
}
