/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-22 11:45 创建
 */
package com.acooly.module.openapi.client.provider.bosc.domain;

import com.acooly.core.utils.Dates;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * @author zhangpu 2017-09-22 11:45
 */
public class BoscRequest extends BoscMessage {

    @JSONField(serialize = false)
    private String userDevice;

    private String timestamp = Dates.format(new Date(), "yyyyMMddHHmmss");


    public String getUserDevice() {
        return userDevice;
    }

    public void setUserDevice(String userDevice) {
        this.userDevice = userDevice;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
