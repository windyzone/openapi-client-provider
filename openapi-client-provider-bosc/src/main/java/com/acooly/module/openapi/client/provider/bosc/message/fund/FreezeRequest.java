package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.FREEZE, type = ApiMessageType.Request)
public class FreezeRequest extends BoscRequest {
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 请求流水号
	 */
	@Size(max = 50)
	private String requestNo;
	/**
	 * 通用冻结请求流水号（若传入，则为“通用冻结”，且不能传入 requestNo）
	 */
	@Size(max = 50)
	private String generalFreezeRequestNo;
	/**
	 * 冻结金额
	 */
	@MoneyConstraint(nullable = false, min = 1, message = "冻结金额不能为空")
	private Money amount;
	
	public FreezeRequest () {
		setService (BoscServiceNameEnum.FREEZE.code ());
	}
	
	public FreezeRequest (String platformUserNo, Money amount) {
		this();
		this.platformUserNo = platformUserNo;
		this.amount = amount;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getGeneralFreezeRequestNo () {
		return generalFreezeRequestNo;
	}
	
	public void setGeneralFreezeRequestNo (String generalFreezeRequestNo) {
		this.generalFreezeRequestNo = generalFreezeRequestNo;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
}