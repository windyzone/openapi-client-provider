package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscNotify;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscAccessTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscAuditStatusEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.PERSONAL_BIND_BANKCARD_EXPAND, type = ApiMessageType.Notify)
public class PersonalBindBankcardExpandNotify extends BoscNotify {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 银行卡号
	 */
	@NotEmpty
	@Size(max = 50)
	private String bankcardNo;
	/**
	 * 见【银行编码】
	 */
	@NotNull
	private BoscBankcodeEnum bankcode;
	/**
	 * 银行预留手机号
	 */
	@NotEmpty
	@Size(max = 50)
	private String mobile;
	/**
	 * 见【鉴权通过类型】
	 */
	@NotNull
	private BoscAccessTypeEnum accessType;
	/**
	 * 换卡状态，固定值：AUDIT（审核中）
	 */
	private BoscAuditStatusEnum auditStatus;
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getBankcardNo () {
		return bankcardNo;
	}
	
	public void setBankcardNo (String bankcardNo) {
		this.bankcardNo = bankcardNo;
	}
	
	public BoscBankcodeEnum getBankcode () {
		return bankcode;
	}
	
	public void setBankcode (BoscBankcodeEnum bankcode) {
		this.bankcode = bankcode;
	}
	
	public String getMobile () {
		return mobile;
	}
	
	public void setMobile (String mobile) {
		this.mobile = mobile;
	}
	
	public BoscAccessTypeEnum getAccessType () {
		return accessType;
	}
	
	public void setAccessType (BoscAccessTypeEnum accessType) {
		this.accessType = accessType;
	}
	
	public BoscAuditStatusEnum getAuditStatus () {
		return auditStatus;
	}
	
	public void setAuditStatus (BoscAuditStatusEnum auditStatus) {
		this.auditStatus = auditStatus;
	}
}