package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscNotify;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscAuditStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscUserRoleEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.ENTERPRISE_REGISTER,type = ApiMessageType.Notify)
public class EnterpriseRegisterNotify extends BoscNotify {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 见【审核状态】
	 */
	@NotEmpty
	private BoscAuditStatusEnum auditStatus;
	/**
	 * 见【用户角色】
	 */
	@NotEmpty
	private BoscUserRoleEnum userRole;
	/**
	 * 银行对公账户
	 */
	@NotEmpty
	@Size(max = 50)
	private String bankcardNo;
	/**
	 * 银行编码
	 */
	@NotEmpty
	@Size(max = 50)
	private BoscBankcodeEnum bankcode;
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public BoscAuditStatusEnum getAuditStatus () {
		return auditStatus;
	}
	
	public void setAuditStatus (BoscAuditStatusEnum auditStatus) {
		this.auditStatus = auditStatus;
	}
	
	public BoscUserRoleEnum getUserRole () {
		return userRole;
	}
	
	public void setUserRole (BoscUserRoleEnum userRole) {
		this.userRole = userRole;
	}
	
	public String getBankcardNo () {
		return bankcardNo;
	}
	
	public void setBankcardNo (String bankcardNo) {
		this.bankcardNo = bankcardNo;
	}
	
	public BoscBankcodeEnum getBankcode () {
		return bankcode;
	}
	
	public void setBankcode (BoscBankcodeEnum bankcode) {
		this.bankcode = bankcode;
	}
}