package com.acooly.module.openapi.client.provider.allinpay;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

@EnableConfigurationProperties({OpenAPIClientAllinpayProperties.class})
@ConditionalOnProperty(value = OpenAPIClientAllinpayProperties.PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientAllinpayConfigration {

    @Autowired
    private OpenAPIClientAllinpayProperties openAPIClientAllinpayProperties;

    @Bean("allinpayHttpTransport")
    public Transport allinpayHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setCharset("GBK");
        httpTransport.setGateway(openAPIClientAllinpayProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientAllinpayProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientAllinpayProperties.getReadTimeout()));
        return httpTransport;
    }

    /**
     * 通联SDK-servlet注册
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean getAllinpayApiSDKServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        ApiServiceClientServlet apiServiceClientServlet = new ApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "allinpayNotifyHandlerDispatcher");
        bean.addInitParameter(ApiServiceClientServlet.SUCCESS_RESPONSE_BODY_KEY, "success");
        List<String> urlMappings = Lists.newArrayList();
        //网关异步通知地址
        urlMappings.add(AllinpayConstants.FIX_NOTIFY_URL+"*");//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(3);
        return bean;
    }
}
