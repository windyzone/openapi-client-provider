/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.cmb.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 *
 * @author zhangpu
 */
@Getter
@Setter
public class CmbResponse extends CmbApiMessage {

	private String status;

	/**
	 * 接口状态码
	 * 成功、失败返回
	 */
	private String code;

	/**
	 * 接口状态描述
	 * 成功、失败返回
	 */
	private String message;

	/**
	 * 接口异常描述
	 * 失败中返回
	 */
	private String sub_code;

	/**
	 * 接口异常描述
	 * 	失败中返回
	 */
	private String sub_msg;
}
