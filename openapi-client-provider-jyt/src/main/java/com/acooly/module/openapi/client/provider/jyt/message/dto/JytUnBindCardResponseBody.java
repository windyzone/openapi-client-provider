package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/6/19 22:06
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytUnBindCardResponseBody implements Serializable {
    /**
     * 交易状态
     解卡验证结果：
     0 通过
     1 拒绝
     */
    @NotBlank
    @XStreamAlias("tran_state")
    private String tranState;

    /**
     * 客户号
     * 持卡人在商户端客户号（一个身份证号对应一个客户号）
     */
    @Size(max = 30)
    @XStreamAlias("cust_no")
    @NotBlank
    private String userId;

    /**
     * 备注信息
     * 结果说明
     */
    @Size(max = 60)
    @XStreamAlias("remark")
    private String remark;
}
