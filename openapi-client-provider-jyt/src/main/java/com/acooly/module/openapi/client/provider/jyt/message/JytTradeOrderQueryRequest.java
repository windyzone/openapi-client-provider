package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytRequest;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytTradeOrderQueryRequestBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/8 17:56
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.TRADE_ORDER_QUERY,type = ApiMessageType.Request)
@XStreamAlias("message")
public class JytTradeOrderQueryRequest extends JytRequest {

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private JytTradeOrderQueryRequestBody tradeOrderQueryRequestBody;

    @Override
    public void doCheck() {
        Validators.assertJSR303(getHeaderRequest());
        Validators.assertJSR303(getTradeOrderQueryRequestBody());
    }
}
